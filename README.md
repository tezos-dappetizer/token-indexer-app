# Token Indexer

## Initial setup

Prerequisites:
-   [Node 16](https://nodejs.org/)
-   [Docker](https://docs.docker.com/get-docker/)
   <br>

1. `npm install -package-lock=false` to take latest packages builds from registries

## Running Locally for Development

1. Run `docker-compose --env-file .env.docker.local up -d` to activate containers with latest indexer (will connect to database schema 'indexer'), postgresql and hasura
2. Create and use `dappetizer.local.config.js` to override default configurations, update the TEZOS_NODE_URL and DB_* related parameters
3. Run indexer by `npm run dev` to run indexer from source code (will connect to database schema 'indexer')
