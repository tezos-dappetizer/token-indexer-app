import { DappetizerConfigUsingDb } from '@tezos-dappetizer/database';
import { TokenIndexerModuleConfig } from '@tezos-dappetizer/token-indexer';

const tokenIndexerModule: TokenIndexerModuleConfig = {
    id: '@tezos-dappetizer/token-indexer',
    config: {
        logIncompatibleContracts: true,
        skipGetMetadata: getEnv('SKIP_GET_METADATA', parseBoolean),
        metadataUpdateParallelism: getEnv('METADATA_UPDATE_PARALLELISM', parseNumber),
    },
};

const config: DappetizerConfigUsingDb = {
    modules: [tokenIndexerModule],
    networks: {
        mainnet: {
            indexing: {
                headLevelOffset: getEnv('HEAD_LEVEL_OFFSET', parseNumber) ?? 2, 
                fromBlockLevel: getEnv('INDEX_FROM_BLOCK_LEVEL', parseNumber) ?? 889027,
                retryIndefinitely: process.env.HALT_ON_CRITICAL_ERROR ? !getEnv('HALT_ON_CRITICAL_ERROR', parseBoolean) : undefined,
            },
            tezosNode: {
                url: getEnv('TEZOS_NODE_URL') ?? 'https://prod.tcinfra.net/rpc/mainnet',
            },
        },
    },
    ipfs: {
        gateways: [
            'https://ipfs.io/',
            'https://cloudflare-ipfs.com/',
            ...getEnv('IPFS_GATEWAY') ? [getEnv('IPFS_GATEWAY')!] : [],
        ],
    },
    database: {
        type: 'postgres',
        host: getEnv('DB_HOST'),
        port: getEnv('DB_PORT', parseNumber),
        username: getEnv('DB_USERNAME') ?? 'postgres',
        password: getEnv('DB_PASSWORD') ?? 'postgrespassword',
        database: getEnv('DB_DATABASE') ?? 'postgres',
        schema: getEnv('DB_SCHEMA') ?? 'indexer',
        synchronize: getEnv('DB_SYNCHRONIZE', parseBoolean) ?? true,
    },
    hasura: {
        dropExistingTracking: getEnv('HASURA_DROP_EXISTING_TRACKING', parseBoolean) ?? false,
        autotrackEntities: getEnv('HASURA_AUTOTRACK_ENTITIES', parseBoolean) ?? true,
        url: getEnv('HASURA_URL'),
        adminSecret: getEnv('HASURA_ADMIN_SECRET'),
        customMappings: [{
            tableName: 'current_balance',
            graphQLTypeName: 'CurrentBalance',
            graphQLFieldName: 'currentBalance',
            columns: [{
                columnName: 'owner_address',
                graphQLName: 'ownerAddress',
            }, {
                columnName: 'amount',
                graphQLName: 'amount',
            }, {
                columnName: 'operation_group_hash',
                graphQLName: 'operationGroupHash',
            }, {
                columnName: 'token_id',
                graphQLName: 'tokenId',
            }, {
                columnName: 'token_contract_address',
                graphQLName: 'tokenContractAddress',
            }, {
                columnName: 'valid_from_block_hash',
                graphQLName: 'validFromBlockHash',
            }],
            relationships: [{
                name: 'token',
                type: 'manyToOne',
                columns: ['token_id', 'token_contract_address'],
                referencedTableName: 'token',
                referencedColumns: ['id', 'contract_address'],
            }],
        }],
        autotrackedMappings: [{
            entityName: 'DbAction',
            graphQLTypeName: 'Action',
            graphQLFieldName: 'action',
            columns: [{
                propertyName: 'type',
                graphQLName: 'actionType',
            }],
        }, {
            entityName: 'DbBalance',
            graphQLTypeName: 'Balance',
            graphQLFieldName: 'balance',
        }, {
            entityName: 'DbContract',
            graphQLTypeName: 'Contract',
            graphQLFieldName: 'contract',
            relationships: [{
                name: 'tokens',
                type: 'oneToMany',
                columns: ['address'],
                referencedTableName: 'token',
                referencedColumns: ['contract_address'],
            }, {
                name: 'contractFeatures',
                type: 'oneToMany',
                columns: ['address'],
                referencedTableName: 'contract_feature',
                referencedColumns: ['contract_address'],
            }],
        }, {
            entityName: 'DbContractFeature',
            graphQLTypeName: 'ContractFeature',
            graphQLFieldName: 'contractFeature',
        }, {
            entityName: 'DbToken',
            graphQLTypeName: 'Token',
            graphQLFieldName: 'token',
        }],
        selectLimit: getEnv('HASURA_SELECT_LIMIT', parseNumber) ?? 100,
        allowAggregations: getEnv('HASURA_ALLOW_AGGREGATIONS', parseBoolean) ?? true,
        namingStyle: getEnv('HASURA_NAMING_STYLE') as any ?? 'noTransformation',
    },
    httpServer: {
        enabled: getEnv('HTTP_SERVER_ENABLED', parseBoolean) ?? true,
        host: getEnv('HTTP_SERVER_HOST'),
        port: getEnv('HTTP_SERVER_PORT', parseNumber),
    },
    logging: {
        console: {
            minLevel: getEnv('LOG_CONSOLE_MIN_LEVEL') as any,
            format: getEnv('LOG_CONSOLE_FORMAT') as any,
        },
        file: {
            minLevel: getEnv('LOG_FILE_MIN_LEVEL') as any ?? 'information',
            format: getEnv('LOG_FILE_FORMAT') as any,
            path: getEnv('LOG_FILE_PATH') ?? 'logs/log.jsonl',
            maxSize: getEnv('LOG_FILE_MAX_SIZE'),
            maxFiles: getEnv('LOG_FILE_MAX_FILES'),
        },
        globalData: {
            env: process.env.ENV,
            network: process.env.NETWORK,
        },
        maxStringLength: getEnv('LOG_MAX_STRING_LENGTH', parseNumber),
    },
    time: {
        longExecutionWarningMillis: getEnv('LONG_EXECUTION_WARNING_MILLIS', parseNumber),
    },
    usageStatistics: {
        enabled: getEnv('USAGE_STATISTICS_ENABLED', parseBoolean) ?? true,
    }
};

export default config;

// Eplicit parsing so that we discover invalid environment variables:

function getEnv(name: string): string | undefined;
function getEnv<T>(name: string, parse: (value: string, name: string) => T): T | undefined;
function getEnv(name: string, parse?: (value: string, name: string) => any): any {
    const value = process.env[name];
    return value // Empty string environment variable is also considered as undefined.
        ? (parse ? parse(value, name) : value)
        : undefined;
}

function parseNumber(rawValue: string, name: string): number {
    const value = Number(rawValue);
    if (isNaN(value)) {
        throw new Error(`Environment variable '${name}' contains invalid number '${rawValue}'.`);
    }
    return value;
}

function parseBoolean(rawValue: string, name: string): boolean {
    switch (rawValue.toLowerCase()) {
        case 'true':
            return true;
        case 'false':
            return false;
        default:
            throw new Error(`Environment variable '${name}' contains invalid boolean '${rawValue}'. It must be 'true' or 'false'.`);
    }
}
